import Foam as fm
import math as m
import matplotlib.pyplot
import numpy as np
import argparse
from scipy.stats import multivariate_normal

import sys
import time
import os
from copy import copy

parser = argparse.ArgumentParser()
parser.add_argument("-n","--ndim",type=int, help="Dimensionality of the gaussian")
parser.add_argument("-a","--av",type=int, help="Number of times this is repeated")
parser.add_argument("--grid",type=str, help="Where to save the foam bubbles to")
args = parser.parse_args()

path = "/share/scratch1/knobbe/toy/banana"
tstr   = "foam_" + str(int(time.time()))
tmpstr = path + "/" + tstr
dirstr = tmpstr
counter = 0
while True:
    if os.path.exists(dirstr):
        counter += 1
        dirstr = tmpstr + "_" + str(counter)
    else:
        break

class Banana():
    def __init__(self, ndim, bananicity=0.1):
        self.b = bananicity
        self.ndim = ndim
        
    def pdf(self, xs):
        #xs = np.array(xs)
        phi = copy(xs)
        phi[:, 1] = phi[:, 1] + self.b*phi[:, 0]**2 - 100*self.b

        return multivariate_normal.pdf(phi, mean=np.zeros(self.ndim), cov=[100]+(self.ndim-1)*[1])

class banana_hypercube:
    def __init__(self,ndim,b=0.1):
        self.ndim = ndim
        self.b = b
        self.banane = Banana(self.ndim,self.b)
        self.points = []
        self.weights = []
        self.plot = False
        
    def __call__(self,xs):

        ys = np.atleast_2d(np.array(xs)) * 60 - 30
        #print(ys)
        weight = self.banane.pdf(ys) * 60**self.ndim
        if self.plot:
            self.points.append(xs)
            self.weights.append(weight)
        return weight

errs_nflow = []
effs_nflow = []

ndim = args.ndim
func = banana_hypercube(ndim)

integrator = fm.Foam()
integrator.Setseed(*np.random.randint(0,1000,2))
integrator.SetDimension(ndim)
integrator.SetNCells(10000)
integrator.SetNOpt(1000)
integrator.SetNMax(20000000)

integrator.SetError(5.0e-8)
integrator.Initialize()
integrator.Integrate(func)

if args.grid:
    integrator.WriteOut(args.grid)
else:
    wgts = []
    trials = int(1e6)
    func.plot = True
    for i in range(trials):
        wgt = integrator.Point()
        wgts.append(wgt)

    '''
    w_arr = np.array(wgts) * np.array(func.weights)
    integral = w_arr.mean()
    stderr = np.sqrt((np.sum(w_arr**2)/trials - integral**2)/(trials-1))
    errs_nflow.append(stderr)
    effs_nflow.append(w_arr.mean() / w_arr.max())
    
    errs_nflow = np.array(errs_nflow)
    effs_nflow = np.array(effs_nflow)
    
    print(errs_nflow)
    print(effs_nflow)
    
    '''
    np.save(dirstr + "_d" +  str(ndim),[errs_nflow,effs_nflow])

