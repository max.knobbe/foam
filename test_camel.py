import Foam as fm
import math as m
import matplotlib.pyplot
import numpy as np
import argparse

import sys
import time
import os

parser = argparse.ArgumentParser()
parser.add_argument("-n","--ndim",type=int, help="Dimensionality of the gaussian")
parser.add_argument("-a","--av",type=int, help="Number of times this is repeated")
args = parser.parse_args()

path = "/share/scratch1/knobbe/toy/camel"
tstr   = "foam_" + str(int(time.time()))
tmpstr = path + "/" + tstr
dirstr = tmpstr
counter = 0
while True:
    if os.path.exists(dirstr):
        counter += 1
        dirstr = tmpstr + "_" + str(counter)
    else:
        break


class Function:
    def __init__(self,ndim):
        self.ndim = ndim
        self.points = []
        self.weights = []
        self.plot = False
        
    def __call__(self,x):
        dx1, dy1, w1=0.25, 0.25, 1./0.2
        dx2, dy2, w2=0.75, 0.75, 1./0.2
        exponent1 = 0
        exponent2 = 0
        prefac =2*(1/w1*np.sqrt(np.pi))**(-self.ndim)

        for i in range(self.ndim):
            exponent1 += (x[i] - dx1)**2
            exponent2 += (x[i] - dx2)**2
        weight = prefac * (m.exp(-w1**2 *exponent1) + m.exp( -w2**2 *exponent2))
>>>>>>> 206b6cdfceb49a945c5d4f1458b320c8e649230a
        if self.plot:
            self.points.append(x)
            self.weights.append(weight)
        return weight


errs_nflow = []
effs_nflow = []

ndim = args.ndim
func = Function(ndim)

integrator = fm.Foam()
integrator.Setseed(*np.random.randint(0,1000,2))
integrator.SetDimension(ndim)
integrator.SetNCells(10000)
integrator.SetNOpt(1000)
integrator.SetNMax(20000000)
integrator.SetError(5.0e-8)
integrator.Initialize()
integrator.Integrate(func)
#integrator.WriteOut("grid.txt")
    
wgts = []
trials = int(1e6)
func.plot = True
for i in range(trials):
    wgt = integrator.Point()
    wgts.append(wgt)

w_arr = np.array(wgts) * np.array(func.weights)
integral = w_arr.mean()
stderr = np.sqrt((np.sum(w_arr**2)/trials - integral**2)/(trials-1))
errs_nflow.append(stderr)
effs_nflow.append(w_arr.mean() / w_arr.max())

errs_nflow = np.array(errs_nflow)
effs_nflow = np.array(effs_nflow)

print(errs_nflow)
print(effs_nflow)

#np.save(dirstr + "_d" +  str(ndim),[errs_nflow,effs_nflow])

    

'''
func = Function()
integrator = fm.Foam()
integrator.SetDimension(2)
integrator.SetNCells(1000)
integrator.SetNOpt(200)
integrator.SetNMax(2000000)
integrator.SetError(5.0e-4)
integrator.Initialize()
integrator.Integrate(func)
integrator.WriteOut("grid.txt")
'''
>>>>>>> 206b6cdfceb49a945c5d4f1458b320c8e649230a
