
# FOAM integrator

Extending https://gitlab.com/shoeche/foam implementation to suit my needs

# Install

The installation only works with python2, the easiest way to install is to
create a virtualenviroment for the installation via
```[bash]
virtualenv -p /usr/bin/python2.7 --distribute venv
```

