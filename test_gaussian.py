import Foam as fm
import math as m
import matplotlib.pyplot
import numpy as np
import argparse

import sys
import time
import os

parser = argparse.ArgumentParser()
parser.add_argument("-n","--ndim",type=int, help="Dimensionality of the gaussian")
parser.add_argument("-a","--av",type=int, help="Number of times this is repeated")
args = parser.parse_args()

path = "/share/scratch1/knobbe/toy/gaussian"
tstr   = "foam_" + str(int(time.time()))
tmpstr = path + "/" + tstr
dirstr = tmpstr
counter = 0
while True:
    if os.path.exists(dirstr):
        counter += 1
        dirstr = tmpstr + "_" + str(counter)
    else:
        break


class Function:
    def __init__(self,ndim):
        self.ndim = ndim
        self.points = []
        self.weights = []
        self.plot = False
        
    def __call__(self,x):
        dx1, dy1, w1=0.5, 0.5, 1./0.2
        mean = 0.5
        w = 1./0.2
        prefac = (1/w*np.sqrt(np.pi))**(-self.ndim)
        exponent = 0
        
        for i in range(self.ndim):
            if x[i] > 0 and x[i] < 1:
                exponent += (x[i] - mean)**2
            else:
                print "not hypercube"

        weight = prefac * m.exp(-w**2 *exponent)
        if self.plot:
            self.points.append(x)
            self.weights.append(weight)
        #weight=m.exp(-w1*((x[0]-dx1)**2+(x[1]-dy1)**2))
        return weight

#exp = np.exp(- ((xs-0.5)**2).sum(axis=-1) / self.alpha**2)

errs_nflow = []
effs_nflow = []

ndim = args.ndim
func = Function(ndim)
integrator = fm.Foam()
integrator.Setseed(*np.random.randint(0,1000,2))
integrator.SetDimension(ndim)
integrator.SetNCells(5000)
integrator.SetNOpt(1000)
integrator.SetNMax(20000000)
integrator.SetError(5.0e-8)
integrator.Initialize()
integrator.Integrate(func)
#integrator.WriteOut("grid.txt")
    
wgts = []
trials = int(1e6)
func.plot = True
for i in range(trials):
    wgt = integrator.Point()
    wgts.append(wgt)

w_arr = np.array(wgts) * np.array(func.weights)
integral = w_arr.mean()
stderr = np.sqrt((np.sum(w_arr**2)/trials - integral**2)/(trials-1))
errs_nflow.append(stderr)
effs_nflow.append(w_arr.mean() / w_arr.max())

errs_nflow = np.array(errs_nflow)
effs_nflow = np.array(effs_nflow)

np.save(dirstr + "_d" +  str(ndim),[errs_nflow,effs_nflow])
